<?php
  if(isset($_POST['submit'])){
    $nama = $_POST['nama'];
    $mapel = $_POST['mapel'];
    $tugas = $_POST['tugas'];
    $uts = $_POST['uts'];
    $uas = $_POST['uas'];

    $nilaitugas = $tugas*0.15;
    $nilaiuts = $uts*0.35;
    $nilaiuas = $uas*0.5;

    
    $hasil =$nilaitugas + $nilaiuts + $nilaiuas;
    

    if($hasil>= 90 && $hasil<=100){
      $grade="A";
    }
    elseif($hasil>70 && $hasil<=90){
      $grade="B";
    }
    elseif ($hasil> 50 && $hasil<=70) {
      $grade="C";
    }
    elseif ($hasil<=50) {
      $grade="D";
    }
  }
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>Nilai Siswa</title>
  </head>
  <style>
    *{
      margin: 0;
      font-family: "Lucida Bright", sans-serif;
    }
    body { 
      background: #003152;
    }
    .form-label{
      color: #003152;
    }
    button{
      width: 100%;
    }
    .container{
      margin-top: 2%;
      width: 50%;
      background-color:white;
      border-radius: 30px;
      box-shadow: 10px 10px 20px rgba(0,0,0,100);
    }
    .alert.alert-success{
      width: 70%;
      color: #003152;
    }
    h1{
      color: white;
      margin-top: 8%;
      font-size: 50px;
    }
    
  </style>
  <body>
    <h1 align="center">FORM INPUT NILAI</h1>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-7 ">
              <form action="" method="POST">
              <br>
              <br>
                  <div class="mb-3">
                    <label for="nama" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama" id="name" placeholder="Masukkan Nama Lengkap...">
                  </div>
                  <div class="mb-3">
                    <label for="mapel" class="form-label">Mata Pelajaran</label>
                    <select class="form-select" name="mapel" id="mapel" aria-label="Default select example" >
                      <option selected>Pilih Mata Pelajaran</option>
                      <option value="Matematika">Matematika</option>
                      <option value="Bahasa Indonesia">Bahasa Indonesia</option>
                      <option value="Bahasa Inggris">Bahasa Inggris</option>
                      <option value="Bahasa Jepang">Bahasa Jepang</option>
                      <option value="Bahasa Korea">Bahasa Korea</option>
                      <option value="PPKN">PPKN</option>
                      <option value="Seni Budaya">Seni Budaya</option>
                      <option value="Fisika">Fisika</option>
                      <option value="Kimia">Kimia</option>
                      <option value="Biologi">Biologi</option>
                      <option value="Penjasorkers">Penjasorkers</option>
                      <option value="Ilmu Pengetahuan Sosial">Ilmu Pengetahuan Sosial</option>
                    </select>
                  </div>
                  <div class="row">
                    <div class="col">
                      <label for="tugas" class="form-label">Nilai Tugas</label>
                      <input type="number" class="form-control" name="tugas" min="0" max="100" id="tugas" placeholder="Masukkan Nilai Tugas...">
                    </div>
                    <div class="col">
                      <label for="uts" class="form-label">Nilai UTS</label>
                      <input type="number" class="form-control" name="uts" id="uts" min="0" max="100" placeholder="Masukkan Nilai UTS...">
                    </div>
                    <div class="col">
                      <label for="uas" class="form-label">Nilai UAS</label>
                      <input type="number" class="form-control" name="uas" id="uas" min="0" max="100" placeholder="Masukkan Nilai UAS...">
                    </div>
                  </div>
                  <br>
                  <center>
                    <button type="submit" name="submit" value="submit" class="btn btn-primary" style= "background-color:#003152;">Hitung</button> 
                  </center> 
              </form>
            </div>
        </div>

        <br>
        <center>
        <?php if(isset($_POST['submit'])) :?>
        <div class="row justify-content-center">
                <div class="col-9">
                  <div class="alert alert-success">
                    Nama Lengkap   : <?php echo $nama?><br>
                    Mata Pelajaran : <?php echo $mapel?><br>
                    Nilai Tugas    : <?php echo $tugas?><br>
                    Nilai UTS      : <?php echo $uts?><br>
                    Nilai UAS      : <?php echo $uas?><br>
                    Total Nilai    : <?php echo $hasil?><br>
                  <h4>  Grade Nilai    : <?php echo $grade?><h4>
                  </div>
                </div>
        </div>
        <?php endif; ?>
        </center>

    </div>
  </body>
</html>